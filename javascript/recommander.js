jQuery(function(){
	(function ($) {
	$(".formulaire_recommander").each(function(){$(this).hide().css("height","").removeClass('hide')});
	$("#recommander>h2, #recommander_bouton, .recommander-actionnable")
		.not('.recommander-clicable')
		.addClass('recommander-clicable')
		.click(function(){
		var $actionnable = $(this);
		var $form;
		if ($actionnable.data('target-form')) {
			$form = $($actionnable.data('target-form'));
		}
		else {
			$form = $actionnable.closest('.recommander-box').find('.formulaire_recommander');
			if (!$form.length) {
				$form = $('.formulaire_recommander').eq(0);
			}
		}
		if ($form.is(':visible')) {
			$form.slideUp();
		}
		else {
			$form.slideDown();
		}
		return false; // si jamais le bouton est un lien
	});
	}(jQuery));
});
