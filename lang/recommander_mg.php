<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/recommander?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'destinataire' => 'Destinataire',

	// R
	'recommander' => 'Recommander cette page',
	'recommander_lecture' => 'Bonjour, @from@ vous recommande la lecture de cette page : ',
	'recommander_message' => 'Recommander',
	'recommander_titre' => 'A lire sur @nom_site@ - '
);
