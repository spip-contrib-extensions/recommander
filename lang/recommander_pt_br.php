<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/recommander?lang_cible=pt_br
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'destinataire' => 'Destinatário',

	// R
	'recommander' => 'Recomendar esta página',
	'recommander_lecture' => 'Bom dia, @from@ recomenda a leitura desta página: ',
	'recommander_message' => 'Recomanda',
	'recommander_titre' => 'Ler em @nom_site@ - '
);
