<?php

/*
 * Plugin Recommander a un ami
 * (c) 2006-2021 Fil&co
 * Distribue sous licence GPL
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

include_spip('inc/filtres');

/**
 * Charger les valeurs du formulaire recommander
 * @param string $titre
 * @param string $url
 * @param string $texte
 * @param string $subject
 * @return array
 */
function formulaires_recommander_charger_dist($titre, $url = '', $texte = '', $subject = '') {
	$valeurs = [
		'recommander_from' => isset($GLOBALS['visiteur_session']['email']) ? $GLOBALS['visiteur_session']['email'] : '',
		'recommander_to' => '',
		'recommander_message' => '',
		'_nospam_encrypt' => true,
	];

	return $valeurs;
}

/**
 * Verifier les valeurs du formulaire recommander
 * @param string $titre
 * @param string $url
 * @param string $texte
 * @param string $subject
 * @return array
 */
function formulaires_recommander_verifier_dist($titre, $url = '', $texte = '', $subject = '') {
	$erreurs = [];

	// on refuse un post en clair
	include_spip('inc/nospam_encrypt');
	$posted = nospam_encrypt_posted('recommander');

	foreach (['recommander_from','recommander_to'] as $c) {
		// on ne veut pas d'un POST direct sous ce nom car trop facile et trop de spam
		set_request($c, $posted[$c] ?? '');

		if (!$email = trim(_request($c))) {
			$erreurs[$c] = _T('form_prop_indiquer_email');
		}
		elseif (
			!email_valide($email)
			// un seul email a la fois
			or strpos($email, ',') !== false
		) {
			$erreurs[$c] = _T('pass_erreur_non_valide', [
				'email_oubli' => htmlspecialchars($email)
				]);
		}
	}

	return $erreurs;
}


/**
 * Envoyer le mail
 * @param string $titre
 * @param string $url
 * @param string $texte
 * @param string $subject
 * @return array
 */
function formulaires_recommander_traiter_dist($titre, $url = '', $texte = '', $subject = '') {

	$subject = sinon(
		$subject,
		_T('recommander:recommander_titre', ['nom_site' => $GLOBALS['meta']['nom_site']])
		. sinon($titre, _request('recommander_titre'))
	);

	$contexte = [
		'titre' => $titre,
		'texte' => $texte,
		'url' => $url ? $url : self(),
		'recommander_from' => _request('recommander_from'),
		'recommander_to' => _request('recommander_to'),
		'recommander_message' => _request('recommander_message'),
	];
	$body = recuperer_fond('modeles/recommander_email', $contexte);
	$header = 'X-Originating-IP: ' . $GLOBALS['ip'] . "\n";

	$to = email_valide(_request('recommander_to'));
	$from = email_valide(_request('recommander_from'));

	$res = [
		'message_ok' => recuperer_fond('modeles/recommander_envoye', $contexte)
	];

	include_spip('inc/nospam');
	$html_confirm = nospam_confirm_action_html('notifications_envoyer_mails', "Recommander a un ami ($from => $to | $url)", [$to, $body, $subject, $from, $header], 'inc/notifications');
	$res['message_ok'] .= ' ' . $html_confirm;

	return $res;
}
